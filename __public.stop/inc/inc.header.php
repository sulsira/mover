    <div class="container">
      
      <!-- start of logo, and nav -->
      <div class="logo">
        <div>
          <a href="index.php"><img src="__public/img/logo.png" class="logo" alt="MOHERST logo"></a>
          <p class="description">
             Moherst Information System
          </p>
        </div><!-- end of logo -->
      </div><!-- end of grid fluid -->
      
      <!-- start of main navigation -->
        <div class="navbar-wrapper">
        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>

            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
              <li class="active selected"><a href="index.php">Home</a></li>
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Sector <b class="caret"></b></a>
              <ul class="dropdown-menu js-activated">
                <li><a href="about_sector">About Sector</a></li>
                <li><a href="login">Login Sector</a></li>
                <!--.dropdown-->
              </ul>
              </li>
             
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Scholarship <b class="caret"></b></a>
              <ul class="dropdown-menu js-activated">
                <li><a href="scholarships">Scholarships</a></li>
                <li><a href="how_to_apply">How to Apply</a></li>
                <li><a href="apply_scholarship">Apply</a></li>
                <!--.dropdown-->
              </ul>
              </li>

             
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Tertiarry & Higher Edu <b class="caret"></b></a>
              <ul class="dropdown-menu js-activated">
                <li><a href="about_higher_edu">Tertiarry & Higher Edu</a></li>
                <li><a href="learning_centers">Leraning Centers</a></li>
                <!--.dropdown-->
              </ul>
              </li>

             
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Research & Development <b class="caret"></b></a>
              <ul class="dropdown-menu js-activated">
                <li><a href="about_research">About Research</a></li>
                <li><a href="register_research">Register</a></li>
                <li><a href="researchers">Researchers</a></li>
                <li><a href="publications">Publications</a></li>
                <!--.dropdown-->
              </ul>
              </li>

             
              <li><a href="sci_tect_ino">Sci Tech & Innovation</a></li>
              <li><a href="login">Login</a></li>
              <li class="dropdown" style="display:none"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Username<b class="caret"></b></a>
              <ul class="dropdown-menu js-activated">
                <li><a href="about-research.php">About Research</a></li>
                <li><a href="register-research.php">Register</a></li>
                <li><a href="researchers.php">Researchers</a></li>
                <li><a href="publications.php">Publications</a></li>
                <!--.dropdown-->
              </ul>
              </li>
            </ul>
            </div>
          </div>
        </div>
    </div>
      <!-- end of main navigation -->

    </div><!-- end of container -->