<?php

class Plot extends \Eloquent {
	protected $primaryKey = 'plot_id';
	protected $fillable = [
		'plot_id',
		'plot_estateID',
		'plot_name',
		'plot_size',
		'plot_price',
		'plot_location',
		'plot_lon',
		'plot_lat',
		'plot_number',
		'plot_status',
		'plot_remarks',
		'plot_availability',
		'plot_cusID',
		'plot_agenID',
		'total_payment',
		'period',
		'Balance',
		'Monthly_Fee',
		'Commencing'
	];

	public function customer(){
		return $this->belongsTo('Customer','plot_id','cust_plotID');
	}
	public function payments(){
		return $this->hasMany('Payment','paym_plotID','plot_id');
	}
	public function estate(){
		return $this->belongsTo('Estate','plot_id');
	}
}


