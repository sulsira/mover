<?php

class Address extends \Eloquent {
	protected $fillable = ['Addr_EntityType', 'Addr_EntityID', 'Addr_EntityType', 'Addr_AddressStreet', 'Addr_POBox', 'Addr_Town', 'Addr_Region', 'Addr_District', 'Addr_Country', 'Addr_CareOf' ];
public function person(){
	return $this->belongsTo('Person','Addr_EntityID','id');
}
}	