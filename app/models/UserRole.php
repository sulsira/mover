<?php

class UserRole extends \Eloquent {
	protected $table = "users_roles";
	protected $fillable = [
'user_id',
'type',
'fullname',
'ip',
'pc',
'privileges',
'department_id',
'userGroup',
'link',
'lickId',
'url',
'deleted'
	];

	public function user(){
		return $this->belongsTo('User','user_id','id');
	}
}