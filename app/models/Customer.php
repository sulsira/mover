<?php

class Customer extends \Eloquent {
	protected $primaryKey = 'cust_id';
	protected $fillable = ['cust_id',
'cust_plotID',
'cust_downpayment',
'cust_partnerID',
'cust_personID',
'cust_ruleID',
'cust_transID',
'cust_status',
'cust_lanID',
'cust_agenID',
'cust_payID',
'cust_deleted',
'cust_visible',
'total_payment',
'period',
'Balance',
'Monthly_Fee',
'Commencing',
'pay_type'
];


	public function person(){
		return $this->belongsTo('Person','cust_personID','id');
	}
	public function plots(){
		return $this->hasMany('Plot','plot_id','cust_plotID');
	}
	public function payments(){
		return $this->hasMany('Payment','paym_custID','cust_id');
	}
	public function transactions(){
		return $this->hasMany('Transaction','trans_custID','cust_id');
	}
	public function documents(){
		return $this->hasMany('Document','entity_ID','cust_id');
	}
}