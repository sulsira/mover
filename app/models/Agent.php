<?php

class Agent extends \Eloquent {
	protected $fillable = ['agen_persID','agen_contID','agen_addrID','agen_prospID'];

	public function person(){
		return $this->belongsTo('Person','agen_persID','id');
	}
}