<?php

class Payment extends \Eloquent {
	protected $fillable = [
		'paym_userID',
		'paym_code',
		'paym_id',
		'paym_custID',
		'paym_paidAmount',
		'paym_balance',
		'paym_currentBal',
		'paym_transDate',
		'paym_deleted',
		'paym_plotID'
	];

	public function plots(){
		return $this->belongsTo('Plot','paym_plotID','plot_id');
	}
	public function customer(){
		return $this->belongsTo('Customer','paym_custID','cust_id');
	}
	public function user(){
		return $this->belongsTo('User','paym_userID','id');
	}
}