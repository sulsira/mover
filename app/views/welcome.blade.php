@include('__partials/doc')
    {{ HTML::style('__public/css/main.css') }}
    {{ HTML::style('__public/css/start-main.css') }}
    {{ HTML::style('__public/css/start-style.css') }}	 
  	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
	<div class="wrapper">
q
		<section>
			@yield('content')
		</section>	
		<footer>
			@yield('footer')
		</footer>	
	</div>
</body>