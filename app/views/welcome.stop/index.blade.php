@include('templates/top')
@section('content')
		<div class="content">
			<div class="signin">
				<h2 class="form-signin-heading">Please sign in</h2>
					<hr>
				<div class="messages">
					@include('flash::message')
					@include('__partials/errors')
				</div>
				{{Form::open(array('route'=>'login_path','class'=>'form-signin','id'=>'','method'=>'POST'))}}
					<div class="input-feild">
						<span class="add-class-name"><strong>	Email</strong> <a href="#" data-toggle="tooltip" title="" data-original-title="A user name unique to you" class="tipify">What is this?</a></span>
						{{Form::input('email','email','',array('class'=>'input-block-level','placeholder'=>"ENTER EMAIL ADDRESS ",'required'=>'required','id'=>'email'))}}
						<span  class="add-class-fee"><strong>Password	</strong></span>
						{{Form::input('password','password','',array('class'=>'input-block-level','placeholder'=>"ENTER PASSWORD",'required'=>'required','id'=>'password'))}}
						<hr>
						{{Form::input('submit','signin','Sign In',array('class'=>'btn btn-large btn-primary btn-block pressed'))}}		        			
					</div>
				{{Form::close()}}
			</div>	

		</div>


		
@stop
@include('templates/bottom')