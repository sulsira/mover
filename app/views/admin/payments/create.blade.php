@include('templates/top-admin')
@section('content')
	<div class="cc">
		<div class="create-Plot">
			<div class="form-snippet">
				<div class="form-header">
					<div class="title">
						<h2>Create A New Payment </h2>
					</div>
				</div>
				<div class="messages">
					@include('__partials/errors');
					 @if(Session::has('success')) 
					  <h3 class="text-success">
					    You have successfully created: <strong> {{ucwords(Session::get('success'))}} </strong>
					  </h3>
					  <hr>
					 @endif
				</div>
				{{Form::open(['route'=>'payments.store'],[],['class'=>'form-snippet'])}}
					<div class="level name">
						<div>
							{{Form::label('plot_name','Plot Name')}}
							{{Form::text('plot_name',null,['class'=>'input-xlarge span6','placeholder'=>'Enter the Plot name','required'=>1])}}
						</div>
						<div>
							{{Form::label('plot_size','Plot Size')}}
							{{Form::text('plot_size',null,['class'=>'input-xlarge span6','placeholder'=>'Plot Size'])}}
						</div>
					</div>
					<div class="level name">
						<div>
							{{Form::label('plot_price','Plot Price (In Dalasis)')}}
							{{Form::text('plot_price',null,['class'=>'input-xlarge span6','placeholder'=>'Enter the Plot price'])}}
						</div>
						<div>
							{{Form::label('plot_location','Plot location')}}
							{{Form::text('plot_location',null,['class'=>'input-xlarge span6','placeholder'=>'Plot location or Adress'])}}
						</div>
					</div>
					<div class="level name">
						<div>
							{{Form::label('plot_number','Plot Number')}}
							{{Form::text('plot_number',null,['class'=>'input-xlarge span12','placeholder'=>'A unique Plot number'])}}
						</div>


					</div>
					<div class="level">
						<div>
							{{Form::label('plot_remarks','Plot Remarks')}}
							{{Form::textarea('plot_remarks',null,['class'=>'input-xlarge span12','placeholder'=>'Enter your remarks here'])}}
						</div>
					</div>
					<div class="level actions">
						<div>
							  <button type="submit" class="btn btn-large btn-primary span12" name="save" value="save">Create Plot</button>
						</div>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')