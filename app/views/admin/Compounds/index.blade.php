@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-compound')
	<div class="c-header cc">
		<h3>Compounds</h3>
	</div>
	<div class="cc">
				<div class="messages">
					@include('flash::message')
					@include('__partials/errors')
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>Owner</th>
							<th>Houses</th>
							<th>Remarks</th>
							<th>created</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if (!empty($compounds)): ?>
							<?php foreach ($compounds as $key => $value): ?>
							<tr>
								<td>
									<?php if ($value['landlord']): ?>
										<a href="{{route('land-lords.show',$value['landlord']['id'])}}">{{ucwords(e($value['landlord']['ll_fullname']))}}</a>
										<?php else: ?>
										No owner
									<?php endif ?>
								</td>
								<td>	
									{{$value['comp_numberOfHouses']}}
								</td>
								<td>	
									{{$value['comp_remarks']}}
								</td>
								<td>	
									{{$value['created_at']}}
								</td>
								<td>	
									<a href="{{route('compounds.show',$value['comp_id'])}}">view</a> |
									<a href="#">Options</a>
								</td>
							</tr>						
							<?php endforeach ?>
							<?php else: ?>
							<tr>
								<td colspan="8"><h4>No Compounds Available!</h4></td>
							</tr>
						<?php endif ?>
					</tbody>
				</table>

		  </div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')