<?php #page specific processing
$ll = Landlord::all();
// $ll = (!empty($ll))? $ll->toArray() : [];

 ?>
@include('templates/top-admin')
@section('content')
	<div class="cc">
		<div class="create-comp">
			<div class="form-snippet">
				<div class="form-header">
					<div class="title">
						<h2>Add a new compound </h2>
					</div>
				</div>
				<div class="messages">
					@include('flash::message')
					@include('__partials/errors')
				</div>
				{{Form::open(['route'=>'compounds.store'],[],['class'=>'form-snippet'])}}
				<div class="level name">
						<div>
							{{Form::label('prop_name','Property Name')}}
							{{Form::text('prop_name',null,['class'=>'input-xlarge span12','placeholder'=>'Enter property name','step'=>'any'])}}
						</div>
				</div>
					<div class="level name">


						<div>
							{{Form::label('ll_id','Land Lord')}}
							<select name="ll_id" class="input-xlarge span6">
								<?php if (!empty($ll)): ?>
									<?php foreach ($ll as $key => $value): ?>
										<option value="{{$value->id}}">{{$value->ll_fullname}}</option>
									<?php endforeach ?>
									<?php else: ?>
									<option>not available</option>
								<?php endif ?>
								
							</select>
						</div>
						<div>
							{{Form::label('comp_houses','Number of houses')}}
							{{Form::number('comp_houses',['class'=>'input-xlarge span6','placeholder'=>'Compound number of houses','step'=>'any'])}}
						</div>
					</div>
					<div class="level name">
						<div>
							{{Form::label('comp_location','Compound location')}}
							{{Form::text('comp_location',null,['class'=>'input-xlarge span6','placeholder'=>'Compound location or Adress'])}}
						</div>
						<div>
							{{Form::label('comp_number','Compound number')}}
							{{Form::text('comp_number',null,['class'=>'input-xlarge span6','placeholder'=>'Compound number or Indentifier'])}}
						</div>
					</div>
					<div class="level">
						<div>
							{{Form::label('comp_remarks','Compound Remarks')}}
							{{Form::textarea('comp_remarks',null,['class'=>'input-xlarge span12','placeholder'=>'Enter your remarks here'])}}
						</div>
					</div>
					<div class="level actions">
						<div>
							  <button type="submit" class="btn btn-large btn-primary span12" name="save" value="save">Create Compound</button>
						</div>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')