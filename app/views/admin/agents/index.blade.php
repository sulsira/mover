@include('templates/top-admin')
@section('content')
	<div class="c-header cc">
		<h3>Agents</h3>
	</div>
	<div class="cc">
		<table class="table">
			<thead>
				<tr>
					<th>Agent Name</th>
					<th>Nationality</th>
					<th>Gender</th>
					<th>Contacts</th>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($agents)): ?>
					<?php foreach ($agents as $key => $value): ?>
					<tr>
							<td>
<a href="{{route('agents.show',$value['agen_id'])}}"><?php echo ucwords($value['person']['pers_fname'] .'  '. $value['person']['pers_mname'].' '.$value['person']['pers_lname']) ?></a>
							</td>
							<td>
<?php echo ucwords($value['person']['pers_nationality']) ?>
							</td>
							<td>
<?php echo ucwords($value['person']['pers_gender']) ?>
							</td>	
							<td>
								<?php if (isset($value['person']['contacts'])): ?>
									<?php if (!empty($value['person']['contacts'])): ?>
										<?php foreach ($value['person']['contacts'] as $key => $value): ?>
											<li><span>{{$value['Cont_ContactType']}} : </span><strong>{{$value['Cont_Contact']}}</strong></li>
											<hr>
										<?php endforeach ?>
									<?php else: ?>
									<li>no Contact info</li>
									<?php endif ?>
								<?php endif ?>
								<ul>
									<li></li>
								</ul>
							</td>			
					</tr>						
					<?php endforeach ?>
					<?php else: ?>
					<tr>
						<td colspan="7"><h4>No Agent Available!</h4></td>
					</tr>
				<?php endif ?>
			</tbody>
		</table>
	</div>
@stop
@include('templates/bottom-admin')