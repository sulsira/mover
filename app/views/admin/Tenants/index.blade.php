@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-tenant')
	<div class="c-header cc">
		<h3>Tenants <a href="#myModal" role="button" data-toggle="modal"><i class="fa fa-plus"></i></a></h3>
	</div>
	<div class="cc">
				<table class="table">
					<thead>
						<tr>
							<th>Fullname</th>
							<th>Nationality</th>
							<th>Gender</th>
							<th>House number</th>
							<th>House price</th>
							<th>created</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if (!empty($tenants)): ?>
							<?php foreach ($tenants as $key => $value): ?>
							<tr>
								<td>
									<a href="{{route('tenants.show',$value['tent_id'])}}">
									{{ucwords(e($value['person']['pers_fname'].' '.$value['person']['pers_mname'].' '.$value['person']['pers_lname']))}}
									</a>								
								</td>
								<td>
									{{ucwords(e($value['person']['pers_nationality']))}}
								</td>
								<td>
									{{ucwords(e($value['person']['pers_gender']))}}
								</td>
								<td>
									{{ucwords(e($value['house']['hous_number']))}}
								</td>
								<td>
									{{ucwords(e($value['house']['hous_price']))}}
								</td>
								<td>
									{{ucwords(e($value['created_at']))}}
								</td>
								<td>
									<a href="{{route('tenants.show',$value['tent_id'])}}">view</a> | 
									<a href="#"> options</a>
								</td>
							</tr>						
							<?php endforeach ?>
							<?php else: ?>
							<tr>
								<td colspan="8"><h4>No Tenance Available!</h4></td>
							</tr>
						<?php endif ?>
					</tbody>
				</table>

		  </div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')