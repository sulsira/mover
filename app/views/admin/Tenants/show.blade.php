<?php #page specific processing 
?>
@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-payment')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="">Tenant Name : {{ucwords($tenant['person']['pers_fname'].' '.$tenant['person']['pers_fname'].' '.$tenant['person']['pers_fname'])}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="#basic">General</a> </li>
                            <li><a href="#rent">Rent</a></li>
                            <li><a href="#payments">Payments</a></li>
                            <li><a href="{{route('houses.show')}}">House</a></li>
                            <li><a href="#notify">Notications</a></li>
                            <li><a href="#documents">Documents</a></li>
                            <li><a href="{{route('tenants.edit')}}">Edit</a> </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <ul class="thumbnails" id="thmb">
                    <li class="span2">
                      <a href="#" class="thumbnail">
                        <img src="http://lorempixel.com/g/200/200/" data-src="holder.js/300x200" alt="">
                      </a>
                    </li>
                </ul>  
            </div>           
        </div>  
    </div>  <!-- end of scope -->

    <div class="content-details clearfix">
            <div class="cc clearfix" >
                <hr>
                <h3>Basic information</h3>
                <hr id="basic">
                <div class="span8 clearfix">
                            <div class="row">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">General information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($tenant['person'])): ?>
                                            <tr>
                                                <td>Fullname:</td>
                                                <td>{{$tenant['person']['pers_fname'].' '.$tenant['person']['pers_fname'].' '.$tenant['person']['pers_fname']}}</td>
                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>Birth day:</td>
                                                <td>{{$tenant['person']['pers_DOB']}}</td>
                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>Gender:</td>
                                                <td> {{$tenant['person']['pers_gender']}} </td>
                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>Nationality:</td>
                                                <td> {{$tenant['person']['pers_nationality']}} </td>
                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>Ethniticity:</td>
                                                <td> {{$tenant['person']['pers_ethnicity']}} </td>
                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                            </tr>   
                                            <?php else: ?>
                                            <tr colspan="3">
                                                <td>There are no general information</td>
                                            </tr>                                         
                                        <?php endif ?>

                                    </tbody>

                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Contact Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($tenant['person']['contacts'])): ?>
                                            <?php foreach ($tenant['person']['contacts']  as $key => $value): ?>
                                                <tr>
                                                    <td>{{$value['Cont_ContactType']}}:</td>
                                                    <td>{{$value['Cont_Contact']}}</td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                                
                                            <?php endforeach ?>
                                        <?php else: ?>
                                        <tr rowspan="3">
                                            <td>There is no contact details</td>
                                        </tr>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($tenant['person']['addresses'])): ?>
                                            <?php foreach ($tenant['person']['addresses'] as $key => $value): ?>
                                                <?php if (!empty($value)): ?>
                                                    <tr>
                                                        <td>Street: </td>
                                                        <td>{{$value['Addr_AddressStreet']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Town: </td>
                                                        <td>{{$value['Addr_Town']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>District: </td>
                                                        <td>{{$value['Addr_District']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>                                                        
                                                <?php endif ?>
                                            <?php endforeach ?>
                                          <?php else: ?>
                                          <tr>
                                            <td>
                                                There is no addresses
                                            </td>
                                          </tr>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                </div>
            </div> <!-- a .cc -->
  <div class="cc clearfix" id="rent">
    <h3>Rent</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>House</th>
            <th>House price</th>
            <th>Monthly fee</th>
            <th>Next payment date</th>
            <th>First paid</th>
            <th>Advance paid</th>
            <th>created</th>
            <th>actions</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($tenant['rents'])): ?>
          <?php foreach ($tenant['rents'] as $key => $rent): ?>
          <?php Session::flash('rentid', $rent['rent_id']); ?>
            <tr>
                <td><a href="{{route('houses.show',$tenant['house']['hous_id'])}}">{{ucwords($tenant['house']['hous_number'])}}</a></td> 
                <td>{{$tenant['house']['hous_price']}}</td> 
                <td>{{$rent['rent_monthlyFee']}}</td> 
                <td><?php echo $rent['rent_nextpaydate'] ?: 'Not paid' ?></td> 
                <td><?php echo $rent['rent_firstmonthpaid'] ?: 'Not paid' ?></td> 
                <td>{{$rent['rent_advance']}}</td> 
                <td>{{$rent['created_at']}}</td> 
                <td><a href="#">options</a></td> 
            </tr>                                              
            <?php endforeach ?>  
        <?php else: ?>
        <tr><td colspan="10"><h4>No rent calculated yet</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
   <div class="cc clearfix" id="payments">
        <h3>Payments  <a href="#myModal" role="button" data-toggle="modal"> <i class="fa fa-plus"></i></a></h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Amount paid</th>
            <th>date paid</th>
            <th>For</th>
            <th>recorded by</th>
            <th>next payment date</th>
            <th>payment per month</th>
            <th>first paid date</th>
            <th>actions</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($tenant['rents'])): ?>
          <?php foreach ($tenant['rents'] as $key => $rent): ?>
            <tr>

            </tr>                                             
            <?php endforeach ?>  
        <?php else: ?>
        <tr>
            <td colspan="8"><h4>No rent payment made yet!</h4></td>
        </tr>
        <?php endif ?>
      </tbody>
    </table>
   </div>
   <div class="cc clearfix" id="notify">
        <h3>Notifications <a href="#add-plot" role="button" data-toggle="modal"> <i class="fa fa-plus"></i></a></h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Title</th>
            <th>Details</th>
            <th>Status</th>
            <th>created</th>
            <th>action</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($customer['plots'])): ?>
          <?php foreach ($customer['plots'] as $key => $plot): ?>
            <tr>
                <td>{{$plot['plot_name']}}</td>
                <td>{{$plot['plot_number']}}</td>
                <td>{{$plot['plot_size']}}</td>
                <td>{{$plot['plot_price']}}</td>
                <td>{{$plot['plot_location']}}</td>
                <td>{{$plot['plot_status']}}</td>
                <td>{{$plot['plot_availability']}}</td>
                <td>{{$plot['plot_remarks']}}</td>
                <td>{{$plot['created_at']}}</td>
                <td><a href="{{route('plots.show',$plot['plot_id'])}}">view</a> | <a href="#">options</a></td>
            </tr>                                                
            <?php endforeach ?>  
        <?php else: ?>
        <tr><td colspan="7"><h4>No plots recorded</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
   </div>
   <div class="cc clearfix" id="documents">
        <h3>Documents <a href="#add-plot" role="button" data-toggle="modal"> <i class="fa fa-plus"></i></a></h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Type</th>
            <th>Remarks</th>
            <th>created</th>
            <th>action</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($customer['plots'])): ?>
          <?php foreach ($customer['plots'] as $key => $plot): ?>
            <tr>
                <td>{{$plot['plot_name']}}</td>
                <td>{{$plot['plot_number']}}</td>
                <td>{{$plot['plot_size']}}</td>
                <td>{{$plot['plot_price']}}</td>
                <td>{{$plot['plot_location']}}</td>
                <td>{{$plot['plot_status']}}</td>
                <td>{{$plot['plot_availability']}}</td>
                <td>{{$plot['plot_remarks']}}</td>
                <td>{{$plot['created_at']}}</td>
                <td><a href="{{route('plots.show',$plot['plot_id'])}}">view</a> | <a href="#">options</a></td>
            </tr>                                                
            <?php endforeach ?>  
        <?php else: ?>
        <tr><td colspan="7"><h4>No plots recorded</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
   </div>

</div>


@stop
@include('templates/bottom-admin')