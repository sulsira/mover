<?php #page specific processing ?>
@include('templates/top-admin')
@section('content')
	<div class="c-header cc">
		<h3><?php echo (!empty($estate))? ucwords($estate['name']) :'Unknow Estate'; ?></h3>
	</div>
	<div class="cc">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Plot Number</th>
					<th>Plot Size</th>
					<th>Plot Price</th>
					<th>Plot Customer</th>
					<th>Plot Location</th>
					<th>Plot Created</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($estate['plots'])): ?>
					<?php foreach ( $estate['plots'] as $key => $value ): ?>
						<tr>
							<td>{{ucwords($key + 1)}}</td>
							<td>{{ucwords($value['plot_number'])}}</td>
							<td>{{ucwords($value['plot_size'])}}</td>
							<td>{{ucwords($value['plot_price'])}}</td>
							<td>
								<?php if (isset($value['customer']) && !empty($value['customer'])): ?>
								<a href="{{route('customers.show',$value['customer']['cust_id'])}}"><?php echo ucwords($value['customer']['person']['pers_fname'] .'  '. $value['customer']['person']['pers_mname'].' '.$value['customer']['person']['pers_lname']) ?></a>
									<?php else: ?>
									no - owner
								<?php endif ?>
							</td>
							<td>
								{{ucwords($value['plot_location'])}}
							</td>
							<td>{{ucwords($value['created_at'])}}</td>	
							<td>
								<a href="{{route('plots.show',$value['plot_id'])}}">view</a> |
								<a href="{{route('plots.edit',$value['plot_id'])}}">edit</a>
							</td>
						</tr>
					<?php endforeach ?>
					<?php else: ?>
					<tr>
						<td colspan="6"><h4>No Plot!</h4></td>
					</tr>
				<?php endif ?>
			</tbody>
		</table>
	</div>
@stop
@include('templates/bottom-admin')