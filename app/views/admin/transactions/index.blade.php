@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-trans')
	<div class="c-header cc">
		<h3>Daily transactions <a href="#myModal" role="button" data-toggle="modal">	<i class="fa fa-plus"></i></a></h3>

	</div>
	<div class="cc">

				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Details</th>
							<th>Date</th>
							<th>By</th>
							<th>Amount</th>
							<th>Remarks</th>
							<th>created</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if (!empty($Partners)): ?>
							<?php foreach ($Partners as $key => $value): ?>
							<tr>
								<td>{{$key+1}}</td>
								<td>{{ucwords($value['name'])}}</td>
								<td><a href="{{route('departments.show',$value['depart_id'])}}">{{ucwords($value['department']['name'])}}</a></td>
								<td><a href="{{route('departments.staffs.show',[$value['depart_id'],$value['staffID']])}}">{{ucwords($value['staff']['Staff_HighestQualification'])}}</a></td>
								<td><?php echo ($value['fee']) ?: 'N/A' ;?></td>
								<td><?php echo ($value['duration']) ?: 'N/A' ;?></td>
								<td><?php echo ($value['qualification']) ?: 'not specified' ;?></td>
								<td><a href="{{route('departments.Partners.show',[$value['depart_id'],$value['id']])}}">View Course</a></td>
							</tr>						
							<?php endforeach ?>
							<?php else: ?>
							<tr>
								<td colspan="8"><h4>No Customers Available!</h4></td>
							</tr>
						<?php endif ?>
					</tbody>
				</table>

		  </div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')