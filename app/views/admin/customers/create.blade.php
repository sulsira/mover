<?php #page specific processing
$plots = Plot::where('plot_availability','=',null)->get(); 
$plots = (!empty($plots))? $plots->toArray() : [];
 ?>
@include('templates/top-admin')
@section('content')
	<div class="cc">
		<div class="create-department">
			<div class="form-snippet">
				<div class="form-header">
					<div class="title">
						<h2>Add A New Customer </h2>
					</div>

				</div>
				<div class="messages">
					@include('flash::message')
					@include('__partials/errors')
				</div>
				{{Form::open(['route'=>'customers.store', 'files' => true],[],['class'=>'form-snippet'])}}

					<div class="level details">

						<div class="first">
							<div>
								{{Form::label('person[fname]','First Name')}}
								{{Form::text('person[fname]',null,['class'=>'input-xlarge','placeholder'=>'Enter first name','required'=>1])}}
							</div>
							<div>
								{{Form::label('person[mname]','Middle Name')}}
								{{Form::text('person[mname]',null,['class'=>'input-xlarge','placeholder'=>'Enter middle name'])}}
							</div>
							<div>
								{{Form::label('person[lname]','Last Name')}}
								{{Form::text('person[lname]',null,['class'=>'input-xlarge','placeholder'=>'Enter last name','required'=>1])}}
							</div>							
						</div>
					</div>
					<div class="level details">
						<span>Bio </span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('person[dob]','Birthday')}}
								{{Form::date('person[dob]',['class'=>'input-xlarge','placeholder'=>'Enter admission date','required'=>1])}}
							</div>

							<div>
								{{Form::label('person[Pers_Nationality]','Nationality')}}
								<select name="person[Pers_Nationality]" id="nation" class="input-xlarge">
									<?php $countries = Variable::domain('Country')->toArray();  ?>
									@foreach ($countries as $key => $country)
									<option>{{$country['Vari_VariableName']}}</option>
									@endforeach
								</select>
							</div>
							<div>
								{{Form::label('person[Pers_Gender]','Gender')}}
								{{Form::select('person[Pers_Gender]', array('male' => 'Male','female' => 'Female'));}}
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Address</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('address[Addr_Town]','Town')}}
								{{Form::text('address[Addr_Town]',null,['class'=>'input-xlarge','placeholder'=>'Enter town'])}}
							</div>
							<div>
								{{Form::label('address[Addr_District]','District')}}
								<select name="address[Addr_District]" id="region">
									<?php $countries = Variable::domain('Addr_District')->toArray();  ?>
									@foreach ($countries as $key => $country)
									<option>{{$country['Vari_VariableName']}}</option>
									@endforeach
								</select> 
							</div>
							<div>
								{{Form::label('address[Addr_AddressStreet]','street')}}
								{{Form::text('address[Addr_AddressStreet]',null,['class'=>'input-xlarge','placeholder'=>'Enter street'])}}
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Contact</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('contact[phones]','Phones')}}
								{{Form::text('contact[phones]',null,['class'=>'input-xlarge','placeholder'=>'Enter phone number(S)'])}}
							</div>
							<div>
								{{Form::label('contact[email]','Email')}}
								{{Form::text('contact[email]',null,['class'=>'input-xlarge','placeholder'=>'Enter Email address'])}} 
							</div>
							<div>
								{{Form::label('contact[telephone]',' Telephone (land line)')}}
								{{Form::text('contact[telephone]',null,['class'=>'input-xlarge','placeholder'=>'Enter numbers'])}}
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Plot details</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('plotID','Plot number')}}
								<select name="cust[cust_plotID]">
								<?php if (!empty($plots)): ?>
									<?php foreach ($plots as $key => $plot): ?>
										<option value="{{$plot['plot_id']}}">{{$plot['plot_number']}}</option>
									<?php endforeach ?>
									<?php else: ?>
									<option value="0">Not Available</option>
								<?php endif ?>
								</select>
								
							</div>
							<div>
								{{Form::label('cust[cust_downpayment]','Down payment')}}
								{{Form::number('cust[cust_downpayment]',['class'=>'input-xlarge','placeholder'=>'Enter down payment in dalasis','required'=>1,'step'=>'any'])}} 
							</div>
							<div>
								{{Form::label('cust[period]','Period in month')}}
								{{Form::number('cust[period]',['class'=>'input-xlarge','placeholder'=>'Enter period in months','required'=>1])}} 
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Payment details</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('cust[pay_commencing]','Commencing')}}
								{{Form::date('cust[pay_commencing]',['class'=>'input-xlarge span6','placeholder'=>'Enter date the payments should start'])}} 
							</div>
							<div>
								{{Form::label('cust[pay_type]','Payment Type')}}
								{{Form::select('cust[pay_type]',['cash'=>'cash','mortgage'=>'mortgage'])}} 
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Identification details</span>
						<hr>
						<div class="first ">
							<div>
								{{Form::label('person[nin_id]','Nation id number / passport number')}}
								{{Form::text('person[nin_id]',null,['class'=>'input-xlarge span6','placeholder'=>'NIN number or passport number'])}} 
							</div>
							<div>
								{{Form::label('photo','Photo')}}
								{{Form::file('photo')}}
							</div>	
							<div>
								{{Form::label('document','Land Document')}}
								{{Form::file('document')}}
							</div>
						</div>
					</div>
					<div class="level details">
						<span>Next of Kin</span>
						<hr>

						<div class="first">
							<div>
								{{Form::label('kin[fname]','First Name')}}
								{{Form::text('kin[fname]',null,['class'=>'input-xlarge','placeholder'=>'Enter first name'])}}
							</div>
							<div>
								{{Form::label('kin[mname]','Middle Name')}}
								{{Form::text('kin[mname]',null,['class'=>'input-xlarge','placeholder'=>'Enter middle name'])}}
							</div>
							<div>
								{{Form::label('kin[lname]','Last Name')}}
								{{Form::text('kin[lname]',null,['class'=>'input-xlarge','placeholder'=>'Enter last name'])}}
							</div>	
							<div>
								{{Form::label('kin[contact]','Contacts')}}
								{{Form::text('kin[contact]',null,['class'=>'input-xlarge','placeholder'=>'Enter contact details'])}}
							</div>						
						</div>
					</div>
					<div class="level actions">
						<div>
							  <button type="submit" class="btn btn-large btn-primary span6" name="save" value="save">Add Customer</button>
						</div>
					</div>
				{{Form::close()}}
			</div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')