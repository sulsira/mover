<?php
// session targeting
// title targeting
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<!-- <meta http-equiv="refresh" content="2" /> -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo 'rs v0.1.1';  ?></title>
    <meta name="description" content="information system">
    <meta name="author" content="Mamadou Jallow <sulsiralumo@gmail.com>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->     
    {{ HTML::style('__public/font-awesome-4.2.0/css/font-awesome.css') }}   
    {{ HTML::style('__public/css/octions/octicons.css') }}   
    {{ HTML::style('__public/css/public.css') }}
    {{ HTML::style('__public/css/main.css') }}
    {{ HTML::style('__public/css/style.css') }}	
</head>
<body>
	<div class="global">
		<div class="main-container">
			<div class="header">
	
			</div><!-- end of header -->
			<div class="container">
				<div class="csidel">
	
				</div><!-- end of csidel -->
				<div class="content">
	
				</div><!-- end of content -->
				<div class="csider">
	
				</div><!-- csider -->
			</div><!-- container -->
			<div class="sbr">
	
			</div><!-- sbr -->
		</div><!-- main-container -->
	</div><!-- global -->
	<!-- start js -->
	<!-- start hiddens -->




<?php #page specific processing
    if(isset($customer) && !empty($customer)):
        $person = $customer['person'];
        $addresses = $person['addresses'];
        $contacts = $customer['person']['contacts'];
        $fullname = ucwords($person['pers_fname'] .'  '. $person['pers_mname'].' '.$person['pers_lname']);
    endif;
 ?>
@include('templates/top-admin')
@section('content')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="">Customer Name : {{ucwords("{ $fullname }")}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                          	<li><a href="#index">General</a> </li>
                          	<li><a href="#transactions">Transactions</a> </li>
                          	<li><a href="#">Edit</a> </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <ul class="thumbnails" id="thmb">
                    <li class="span2">
                      <a href="#" class="thumbnail">
                        <img src="http://lorempixel.com/g/200/200/" data-src="holder.js/300x200" alt="">
                      </a>
                    </li>
                </ul>  
            </div>           
        </div>  
    </div>  <!-- end of scope -->
    <div  id="index"></div>
	<div class="cc">
        <!-- <hr> -->
    	<div>
        <hr>
    		<div class="aside bio">
                <div class="ch">
                    <h4 id="bio">Bio</h4>
                </div>
                <hr>
                <div class="details">
                    <div class="aside left span8">
                        <center>
                            <strong>{{ucwords($fullname)}}</strong>
                            <hr>
                            <div class="thumbnail span3 thm">
                                <img src="http://lorempixel.com/g/200/200/" data-src="holder.js/300x200" alt="">
                            </div>
                            <hr>
                            <div class="row">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">General information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Fullname:</td>
                                            <td>{{ucwords($fullname)}}</td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Birth day:</td>
                                            <td>{{ucwords($person['pers_DOB'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td> {{ucwords($person['pers_gender'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Nationality:</td>
                                            <td> {{ucwords($person['pers_nationality'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Ethniticity:</td>
                                            <td> {{ucwords($person['pers_ethnicity'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Contact Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($contacts)): ?>
                                            <?php foreach ($contacts  as $key => $value): ?>
                                                <tr>
                                                    <td>{{ucwords($value['Cont_ContactType'])}}:</td>
                                                    <td>{{ucwords($value['Cont_Contact'])}}</td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                                
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($addresses)): ?>
                                            <?php foreach ($addresses as $key => $value): ?>
                                                <?php if (!empty($value)): ?>
                                                    <tr>
                                                        <td>Street: </td>
                                                        <td>{{$value['Addr_AddressStreet']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Town: </td>
                                                        <td>{{$value['Addr_Town']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>District: </td>
                                                        <td>{{$value['Addr_District']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>                                                        
                                                <?php endif ?>
                                            <?php endforeach ?>
                                          
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Plots</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($customer['plots'])): ?>
                                          <?php foreach ($customer['plots'] as $key => $plot): ?>
                                            <tr>
                                                <td>name:</td>
                                                <td>{{ucwords($plot['plot_name'])}}</td>
                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                            </tr>                                                
                                            <?php endforeach ?>  
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                        </center>
                    </div>
                    <div class="aside span10">
                        <div class="hed">
                            <h3>A add daily Transaction</h3>
                        </div>
                        <hr>
                        <div class="messages">
                            @include('flash::message')
                            @include('__partials/errors')
                        </div>
                        {{Form::open(['route'=>'payments.store','method'=>'POST'])}}
                        {{Form::hidden('paym_cusID',$customer['cust_id'])}}
                        {{Form::hidden('paym_currentBal',(is_numeric($customer['cust_downpayment']))? $customer['cust_downpayment'] : 0)}}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Amount Paid
                                            </label>
                                        {{Form::number('paym_paidAmount',['placeholder'=>'Enter the amount paid','min'=>"1", 'step'=>"any"])}}
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">
                                                Payment Date
                                            </label>
                                            {{Form::date('paym_transDate',['placeholder'=>'Enter the amount paid'])}}
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">
                                                Plots
                                            </label>
                                            <select name="paym_plotID">
                                            <?php if (!empty($customer['plots'])): ?>
                                              <?php foreach ($customer['plots'] as $key => $plot): ?>
                                                    <option value="{{$plot['plot_id']}}">{{ucwords($plot['plot_name'])}}</option>
                                                                                             
                                                <?php endforeach ?>  
                                            <?php endif ?>
                                            </select>
                                        </div>

                                    </div>
             
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <button class="btn btn-green btn-block" type="submit">
                                            Update <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                </div>
                           {{Form::close()}}
                    </div>
                </div>
    		</div>	<!--#bio-->	

    	</div> <!-- #index -->
    </div> <!-- a .cc -->
  <div class="cc" id="transactions">
    <h3>Transactions Records</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>plot</th>
            <th>date</th>
            <th>Amount</th>
            <th>recorded by</th>
            <th>initial balance</th>
            <th>due balance</th>
            <th>current balance</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($customer['plots'])): ?>
          <?php foreach ($customer['plots'] as $key => $plot): ?>
            <tr>
                
            </tr> 
                <?php foreach ($plot['payments'] as $key1 => $value1): ?>
                    <tr>
                        <td><a href="">{{ucwords($plot['plot_name'])}}</a></td>
                        <td>{{e($value1['paym_transDate'])}}</td>
                        <td>{{e($value1['paym_paidAmount'])}}</td>
                        <td>{{e($value1['user']['email'])}}</td>
                        <td>{{e($value1['paym_balance'])}}</td>
                        <td>--</td>
                        <td>--</td>
                        <td>{{e($value1['created_at'])}}</td>
                    </tr>                             
                <?php endforeach ?>                                               
            <?php endforeach ?>  
        <?php endif ?>
      </tbody>
    </table>
  </div>
@stop
@include('templates/bottom-admin')


</body>
</html>