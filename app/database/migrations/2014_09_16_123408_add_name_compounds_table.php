<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNameCompoundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('compounds', function(Blueprint $table)
		{
			$table->string('name')->nullable()->after('comp_size');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('compounds', function(Blueprint $table)
		{
			$table->dropColumn('name');
		});
	}

}
