<?php namespace services\Validators;


class Plot extends Validate{

		public static $rules = [
		// 'plot_name'=>'required|max:200',
		'plot_size'=>'required|max:200',
		'plot_price'=>'required|numeric',
		'plot_location'=>'max:200',
		'plot_number'=>'max:200',
		'plot_remarks'=>'max:200'
	];
	public function __construct($attributes = null){
		$this->attributes = $attributes ?: \Input::all();
	}
}