<?php
use Intervention\Image\ImageManagerStatic as Image;
class CustomersController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /customers
	 *
	 * @return Response
	 */
	public function index()
	{
		$cus  = Customer::with('person.contacts','plots')->get();
		$customers = $cus ? $cus->toArray() : [];
		$this->layout->content = View::make('admin.customers.index')->with('customers',$customers);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /customers/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.customers.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /customers
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$person = array();
		$staff = array();
		$address = array();
		$contact = array();
		$customer = array();
		$done = false;
		// dd($input);
		if ($input) :
			foreach ($input as $k => $table) {
				if (is_array($table)) {
					if ($k == 'person') {
						$V = new services\validators\Person($table);
						if($V->passes()){
							$person = Person::create(array(
							'pers_fname' => $table['fname'], 
							'pers_mname' => ($table['mname']) ?: null, 
							'pers_lname' => $table['lname'], 
							'pers_type' => 'Customer', 
							'pers_DOB' => $table['dob'], 
							'pers_gender' => $table['Pers_Gender'], 
							'pers_nationality' => $table['Pers_Nationality'], 
							'pers_NIN' => $table['nin_id']
							));
							if ($person->id) {
								$done = true;
							}
						}else{
							$errors = $V->errors;
							return Redirect::back()->withErrors($errors)->withInput();							
						}


					}
					if ($k == 'address') {
						$address = $table;
						if ($person->id) {
							$address = array_add($address, 'Addr_EntityID', $person->id);
							$address = array_add($address, 'Addr_EntityType', 'Person');
							$V = new services\validators\Address($table);
							if($V->passes()){
								$address = Address::create($address);
							}
						}
						$errors = $V->errors;

					}
					if ($k == 'contact') {

						

						if ($person->id) {

							$V = new services\validators\Contact($table);
							foreach ($table as $key => $value) {
								if($V->passes()){

									if(!empty($value)){
										$contact = Contact::create(array(
										'Cont_EntityID' => $person->id,	
										'Cont_EntityType' => 'Person',	
										'Cont_Contact' => $value,	
										'Cont_ContactType' =>  $key	
										));

									$contact = $contact->toArray();
									}
								}
							}
							$errors = $V->errors;
						}
					}
				if ($k == 'cust') {
					#loop hole here not comming in values controlls
					$price  = Plot::find($table['cust_plotID'])->plot_price;
					$customer   = Customer::create(array(
						'cust_personID'=> $person->id,
						'cust_plotID'=> ($table['cust_plotID']) ?: 0, #view drop down select
						'cust_downpayment'=> $table['cust_downpayment'],
						'period' => $table['period'],
						'pay_type' => $table['pay_type'],
						'Monthly_Fee' => ($price - $table['cust_downpayment']) / $table['period'],
						'Commencing' => $table['pay_commencing']
					));
					if (!empty($customer)) {

						$plot = Plot::find( $customer->cust_plotID );
						$plot->plot_availability = 'not-available';
						// $plot->plot_cusID = $customer->cust_id;
						$plot->save();
					}
					}
				if ($k ==  'kin') {
					Kin::create(array(
						'fname' => $table['fname'],
						'mname' => $table['mname'],
						'lname' => $table['lname'],
						'contacts' => $table['contact'],
						'customer_id' => $customer->cust_id
					));
				}


				}
			} #end of loop

			if($done){
				

					if ($input['photo']) {

							$not = 'media'.DS.'customer'.DS.$person->pers_fname.'_'.$person->pers_mname.'_'.$person->pers_lname.'_'.$person->id;
							$foldername = DS.$not.DS;
							// $full_dir = $foldername.'img'.DS.'full';

							$path = base_path().$foldername ;
							$thumbnail_dir = $not.DS.'img'.DS.'thumbnail'.DS;
							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$filename = Input::file('photo')->getClientOriginalName();
							// dd($path);
							$shortened = base_convert(rand(10000,99999), 10, 36);
							$rename = $person->pers_fname.'_'.$shortened.'_photo_.'.$filename;
							$filetyp = Input::file('photo')->getMimeType();
							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$uploaded = false;

							if ((File::exists($path)) && ( File::exists( $path.DS.'img'.DS ) ) && ( File::exists( $path.DS.'img'.DS.'thumbnail') ) ) {

								Input::file('photo')->move($path.DS.'img'.DS, $rename);

									#notice #01
										// there was a problem when trying to resize the image. the fix is go in the project dir (html but ! include)

									// we resize (250 X 300)
									$img = Image::make($not.$rename);
									$img->resize(250 , 300);
									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => 'profile picture' ,
										'entity_type' => 'Customer',
										'entity_ID' => $customecust_r->id,
										'type' => 'Photo',
										'fullpath' => $path.DS.'img'.DS.$rename,
										'filename' => $filename,
										'foldername' => $foldername,
										'extension' => $fileexten,
										'filetype' => $filetyp,
										'thumnaildir' => $thumbnail_dir.$rename
									));
								
								# code...
							}else{
								mkdir($path);
								mkdir($path.DS.'img'.DS);
								mkdir($path.DS.'img'.DS.'thumbnail');
								Input::file('photo')->move($path.DS.'img'.DS , $rename);
									// we resize (250 X 300)
									// dd($not.DS.$rename);
									$img = Image::make($not.DS.'img'.DS.$rename);
									$img->resize(250 , 300);
									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => 'profile picture' ,
										'entity_type' => 'Customer',
										'entity_ID' => $customer->cust_id,
										'type' => 'Photo',
										'fullpath' =>  $path.DS.'img'.DS.$rename,
										'filename' => $filename,
										'foldername' => $foldername,
										'extension' => $fileexten,
										'filetype' => $filetyp,
										'thumnaildir' => $thumbnail_dir.$rename
									));
							}
					} #end of photo


					if ($input['document']) {


							$not = 'media'.DS.'customer'.DS.$person->pers_fname.'_'.$person->pers_mname.'_'.$person->pers_lname.'_'.$person->id;
							$foldername = DS.$not.DS;
							// $full_dir = $foldername.'img'.DS.'full';
							
							$path = base_path().$foldername ;
							$fileexten = Input::file('document')->getClientOriginalExtension();
							$filename = Input::file('document')->getClientOriginalName();
							// dd($path);
							$shortened = base_convert(rand(10000,99999), 10, 36);
							$rename = $person->pers_fname.'_'.$shortened.'_document_.'.$filename;
							$filetyp = Input::file('document')->getMimeType();
							$fileexten = Input::file('document')->getClientOriginalExtension();
							$uploaded = false;

							if ( File::exists($path)  && ( File::exists( $path.DS.'document'.DS ) ) )  {

								Input::file('document')->move($path , $rename);

									#notice #01
										// there was a problem when trying to resize the image. the fix is go in the project dir (html but ! include)

									// we resize (250 X 300)
									// $img = Image::make($not.$rename);
									// $img->resize(250 , 300);
									// $img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => $filename ,
										'entity_type' => 'Customer',
										'entity_ID' => $customer->cust_id,
										'type' => 'Document',
										'fullpath' => $path.$rename,
										'filename' => $filename,
										'foldername' => $foldername,
										'extension' => $fileexten,
										'filetype' => $filetyp
									));
								
								# code...
							}else{
								File::exists($path) ?: mkdir($path);
								mkdir($path.DS.'document'.DS);
								Input::file('document')->move($path.DS.'document'.DS , $rename);
									// we resize (250 X 300)
									// $img = Image::make($not.$rename);
									// $img->resize(250 , 300);
									// $img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => $filename ,
										'entity_type' => 'Customer',
										'entity_ID' => $customer->cust_id,
										'type' => 'Document',
										'fullpath' => $path.$rename,
										'filename' => $filename,
										'foldername' => $foldername,
										'extension' => $fileexten,
										'filetype' => $filetyp
									));
							}
					} #end of photo
				Flash::message("Successfully added a Customer");
				return Redirect::back();
			}else{
				return Redirect::back()->withErrors($errors)->withInput();							
			}
		endif;
	}

	/**
	 * Display the specified resource.
	 * GET /customers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// $cus = Customer::with('documents')->get();
		$cus = Customer::with('person.contacts','person.addresses','plots.payments.user','transactions','documents')->where('cust_id','=',$id)->first();
		$customer = $cus ? $cus->toArray() : []; #secure

		// dd($customer);

		$this->layout->content = View::make('admin.customers.show')->with('customer', $customer);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /customers/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /customers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /customers/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}