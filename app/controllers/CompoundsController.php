<?php
	/** Topics
		1. Notices
			- no validation 
	 * 			#notice 01
	 *  
	 *
	 * @return Response {}
	 */
class CompoundsController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /compounds
	 *
	 * @return Response
	 */
	public function index()
	{
		$all = Compound::with('landlord')->get();
		$all = ($all) ? $all->toArray() : [];
		$this->layout->content = View::make('admin.Compounds.index')->with('compounds',$all);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /compounds/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.Compounds.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /compounds
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		if(Request::ajax()):
			$data = array();
			$fname = '';
			$mname = '';
			$lname = '';
			$keywords = [];
			if (str_contains($input['fullname'], ' ')) {
				
				$keywords = preg_split("/[\s]+/", trim($input['fullname']));
				// $t = count($keywords);
				// if ( $t > 2) {
				// 	for($i = 0; $i >= $t; $i++){
				// 		if ($i != 0 && $i != $t) {
				// 			$mname .= $keywords[$i].' ';
				// 		}else{
				// 			if($i != 0){
				// 				$fname .= $keywords[$i];	
				// 			}else{
				// 				$lname .= $keywords[$i];	
				// 			}
				// 		}
				// 	}
				// }
			}
			$person = Person::create(array(
					'pers_fname'=> (head($keywords)) ?: '',
					'pers_mname'=> ($keywords[1]) ?: '',
					'pers_lname'=> (last($keywords)) ?: '',
					'pers_type'=> 'Landlord'
			));
			$ll = Landlord::create(array('ll_fullname' =>$input['fullname'],'ll_personid'=>$person->id));
			$data['compound'] = Compound::create(array(
				'comp_landLordID' => $ll->id,
				'comp_numberOfHouses' => e($input['Compound_houses']),
				'location' => e($input['Compound_location'])
			));

			$data['contact'] = Contact::create(array(
				'Cont_EntityID' => $person->id,	
				'Cont_EntityType' => 'Person',	
				'Cont_Contact' => e($input['contacts']),	
				'Cont_ContactType' =>  'Phone'
			));

			$data['address'] = Address::create(array(
				 'Addr_EntityID' => $person->id,	
				 'Addr_EntityType' => 'Person', 
				 'Addr_AddressStreet' => e($input['address']),				
			));
			$data['landlord'] = $ll;
			return Response::json( $data );

		endif;

		#notice 0.1
		// add compound v0.0.1 #no validation
		$compound =  Compound::create(array(
			'comp_numberOfHouses'=> $input['comp_houses'],
			'comp_size'=> $input['comp_size'],
			'comp_remarks'=> $input['comp_remarks'],
			'location'=> $input['comp_location'],
			'comp_indentifier'=> $input['comp_number']		
		));
		if( $compound ){
				Flash::message("Successfully added a Compound");
				return Redirect::back();
			}else{
				return Redirect::back()->withErrors($errors)->withInput();							
			}
	}

	/**
	 * Display the specified resource.
	 * GET /compounds/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /compounds/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /compounds/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /compounds/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}