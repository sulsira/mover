<?php

class TenantsController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /tenants
	 *
	 * @return Response
	 */
	public function index()
	{
		// $all = House::with('tenants')->get();
		$all = Tenant::with('house','person.contacts')->get();
		$all = ($all)? $all->toArray() : [];
		$this->layout->content = View::make('admin.Tenants.index')->with('tenants',$all);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tenants/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.Tenants.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tenants
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$person = array();
		$staff = array();
		$address = array();
		$contact = array();
		$done = false;
		if ($input) :
			foreach ($input as $k => $table) {
				if (is_array($table)) {
					if ($k == 'person') {
						$V = new services\validators\Person($table);
						if($V->passes()){
							$person = Person::create(array(
							'pers_fname' => $table['fname'], 
							'pers_mname' => ($table['mname']) ?: null, 
							'pers_lname' => $table['lname'], 
							'pers_type' => 'Tenant', 
							'pers_DOB' => $table['dob'], 
							'pers_gender' => $table['Pers_Gender'], 
							'pers_nationality' => $table['Pers_Nationality'], 
							'pers_ethnicity' => $table['Pers_Ethnicity'], 
							));
							if ($person->id) {
								$done = true;
							}
						}else{
							$errors = $V->errors;
							return Redirect::back()->withErrors($errors)->withInput();							
						}


					}
					if ($k == 'address') {
						$address = $table;
						if ($person->id) {
							$address = array_add($address, 'Addr_EntityID', $person->id);
							$address = array_add($address, 'Addr_EntityType', 'Person');
							$V = new services\validators\Address($table);
							if($V->passes()){
								$address = Address::create($address);
							}
						}
						$errors = $V->errors;

					}
					if ($k == 'contact') {

						

						if ($person->id) {

							$V = new services\validators\Contact($table);
							foreach ($table as $key => $value) {
								if($V->passes()){

									if(!empty($value)){
										$contact = Contact::create(array(
										'Cont_EntityID' => $person->id,	
										'Cont_EntityType' => 'Person',	
										'Cont_Contact' => $value,	
										'Cont_ContactType' =>  $key	
										));

									$contact = $contact->toArray();
									}
								}
							}
							$errors = $V->errors;
						}
					}
				if ($k == 'cust') {
					#loop hole here not comming in values controlls
					// dd($input);
					// Tenant::create(array(
					// 		'tent_houseID'=> $input[''],
					// 		'tent_compoundID'=> $input[''],
					// 		'tent_rentID'=> $input[''],
					// 		'tent_advance'=> $input[''],
					// 		'tent_monthlyFee'=> $input[''],
					// 		'tent_paymentStype'=> $input[''],
					// 		'tent_status'=> $input['']
					// ));

					}

				}
			}
			if($done){

			$house = House::where('hous_id','=',$input['houseID'])->first();
			$house = $house ?: $house->toArray();
			$tent = Tenant::create(array(
					'tent_houseID'=> $input['houseID'],
					'tent_compoundID'=> $house['hous_compoundID'],
					'tent_advance'=> $input['down_payment'],
					'tent_personid'=> $person->id
			));
			// we add rent 
			$montlyfee = $input['Monthly_fee'] ?:  $house['hous_price'] ;
			Rent::create(array(
				'rent_houseID' => $input['houseID'],
				'rent_tenantID' => $tent->id,
				'rent_advance' => $input['down_payment'],
				'rent_firstmonthpaid' => null,
				'rent_nextpaydate' => null,
				'rent_monthlyFee' => $montlyfee
			));

				Flash::message("Successfully added a Tenant");
				return Redirect::back();
			}else{
				return Redirect::back()->withErrors($errors)->withInput();							
			}
		endif;
	}

	/**
	 * Display the specified resource.
	 * GET /tenants/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// $all = Rent::with('payments')->first();
		$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments')->whereRaw('tent_id = ? AND deleted = ? AND tent_status = ?',[$id,0,0])->first();
		$all = ($all)? $all->toArray() : [];
		$this->layout->content = View::make('admin.Tenants.show')->with('tenant',$all);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tenants/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tenants/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tenants/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}