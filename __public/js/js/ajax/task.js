(function($){
var Task = {
	init: function(config){
		// MAKING DOM AVAILABLE TO THE OBJECT
		this.container = config.container;
		this.userselection = config.userselection;
		this.form = config.form;

		// METHODS PULL DOM IN
		this.events.change.call(this);
		this.events.submit.call(this);
		this.events.click.call(this);

		// AJAX SETUP FOR ALL REQUEST
		$.ajaxSetup({
			url: 'tasks',
			type: 'POST',
			dataType: 'json'
		});
	},
	events: {		
		change: function(){
			var $this = this;
			$this.userselection.on('change',$this.option);
		},
		submit: function(){
			var $this = this;
			$this.form.on('submit',$this.sendData);
		},
		click: function(){
			var $this = this;
			$this.form.find('input.checkbox').on('click',function(){
				var sib = $(this).siblings();
				var sibval = sib.val() || sib.data('val');
				// check and see if its checked 
				// if yes then return check for the other input if yes 
				// do nothing else do something
				// console.log(this)
				// if ($(this).is(':checked')) {
					
				// 	if ($(sib).is('input')) {
				// 		$(sib).detach();
				// 		$(this).after('<span data-val="'+sibval+'">'+'</span>');
				// 		return;
				// 	};
				// 		$(sib).detach();
				// 		$(this).after('<input	type="hidden" name="userId" data-input=""  value="'+sibval+'"/>');
				// 		return;

				// }else{
				// 	$(sib).detach();
				// 	$(this).after('<span data-val="'+sibval+'">'+'</span>');
				// }
			});
		}
	},
	option: function(){
		var $this = Task;
		var value = $(this).find(':selected').attr('value');
		if ($.isNumeric(value)) {
			$this.getUserById(value).then(function(e){
				$this.buildTable(e);
			});
		}else{
			$this.getUsers(value).then(function(e){
				$this.buildTable(e);
			});
		}
	},
	getUsers: function( type ){
	var self = this;
	var dfd = new $.Deferred();
	$.ajax({
		data: {type:type},
		success: dfd.resolve,
		error: self.errored,
		beforeSend: self.loading,
		complete: self.completed
	});
	return dfd.promise();
	},
	getUserById: function( id ){
	var self = this;
	var dfd = new $.Deferred();
	$.ajax({
		data: {pid:id},
		success: dfd.resolve,
		error: self.errored,
		beforeSend: self.loading,
		complete: self.completed
	});
	return dfd.promise();
	},
	getUserById: function( id ){
	var self = this;
	var dfd = new $.Deferred();
	$.ajax({
		data: {pid:id},
		success: dfd.resolve,
		error: self.errored,

		beforeSend: self.loading,

		complete: self.completed
	});
	return dfd.promise();
	},
	loading: function(){

	},
	completed: function(){

	},
	buildTable: function(data){
		var self = Task;
		if (typeof data !== 'undefined' || $(data).length > 0) {
			var template = $.trim( $('#tabletrusers').html() );
			var table = $(self.container).find('table#displayusers');
			var tbody = $(table).find('tbody');
			var tchildren =  $(tbody).children();
			if ( tchildren.length > 0) {
				$.each(tchildren, function(ind, obj ){
					$(obj).detach();
				});
			};
			if ($(data).length > 1) {
					$.each(data, function(index, obj){

						temp = 
								template.replace( /{name}/ig, obj.fullname )
										.replace( /{directrate}/ig, obj.userGroup )
										.replace( /{status}/ig, obj.status )
										.replace( /{userId}/ig, index + '_userId' )
										.replace( /{id}/ig, obj.id );
						$(table).find('tbody').append(temp);
					});
				}else{
				$.each(data, function(index, obj){
					temp = 
						template.replace( /{name}/ig, data.fullname )
								.replace( /{directrate}/ig, data.userGroup )
								.replace( /{status}/ig, data.status )
								.replace( /{userId}/ig, index + '_userId' )
								.replace( /{id}/ig, data.id );
				
				});	
			$(table).find('tbody').append(temp);
			}	
		}else{
			self.errored("array is empty");
		}
	self.events.click.call(self);
	},
	errored: function(message){
	},
	sendData: function(e){
		// e.preventDefault();
		var self = this;
		var dfd = new $.Deferred();
		$.ajax({
			data:  $(this).serialize(),
			success: function(e){
				// dfd.resolve
				console.log(e);
			},
			error: function(e,a,c){
				console.log(c);
			}
		});
		return dfd.promise();
	}

};
Task.init({
	container : $('#myModal'),
	userselection : $('#userselection'),
	form : $('form#task')
});


})(jQuery);